"""Homework 8 Task 2 implementation."""

from pathlib import Path
import shelve
from typing import Any, Generator


def get_number(prompt: str) -> int:
    """Get an integer from input."""

    while True:
        try:
            number = int(input(f"{prompt}: "))
            return number
        except ValueError as err:
            print(f"!Invalid input: {err}")


def choose_from_list(items: list, prompt: str) -> Any:
    """Choose an item from the list by its index."""

    items_str = '\n\t'.join([
        f"'{idx}' - {item}" for idx, item in enumerate(items)
    ])
    while True:
        item_idx = get_number(
            f"{prompt}:\n\t{items_str}\n"
        )
        try:
            return items[item_idx]
        except IndexError as err:
            print(f"!Invalid input: {err}")


def add_link(db_file: str, full_link: str, brief_link: str) -> None:
    """Add a new link to the database."""
    with shelve.open(db_file) as db:
        db[brief_link] = full_link


def all_links(db_file: str) -> Generator:
    """Return all links in the database."""
    with shelve.open(db_file) as db:
        for brief_link, full_link in db.items():
            yield brief_link, full_link


def find_link(db_file: str, brief_link: str) -> str:
    """Find a link in the database."""
    with shelve.open(db_file) as db:
        return db[brief_link]


def main(db_file: str) -> None:
    """Display the main menu and perform the actions."""

    # create DB file if not exists
    db_file_path = Path(db_file)
    if not db_file_path.is_file():
        db_file_path.parent.mkdir(parents=True, exist_ok=True)
        with shelve.open(db_file):
            pass

    print("\nWelcome to the BRIEF LINKS SERVICE!")

    while True:
        actions = [
            "Quit",
            "Add a new link",
            "Show all links",
            "Find a link",
        ]
        command = choose_from_list(actions, "\nMAIN MENU")

        match command:
            case 'Quit':
                print("Bye!")
                break
            case 'Add a new link':
                full_link = input("Enter the full link: ")
                brief_link = input("Enter its short name: ")
                add_link(db_file, full_link, brief_link)
                print(f"Added: {brief_link} --> {full_link}")
            case 'Show all links':
                for brief_link, full_link in all_links(db_file):
                    print(f"{brief_link} --> {full_link}")
            case 'Find a link':
                brief_link = input("Enter the link short name: ")
                try:
                    full_link = find_link(db_file, brief_link)
                except KeyError:
                    full_link = 'Nothing found'
                print(f"{brief_link} --> {full_link}")


if __name__ == '__main__':

    db_file_name = './links/db-brief-links'
    main(db_file_name)
