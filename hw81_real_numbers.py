"""Homework 8 Task 1 implementation."""

import random
from typing import Generator


def save_numbers_to_file(
        filename: str, quantity: int = 10000, limit: int = 100
) -> float:
    """Save random real numbers to a file. Return control sum."""

    def real_numbers(n: int, max_number: int) -> Generator:
        """Yield N random real numbers in range [-max_number, max_number]."""
        for i in range(n):
            yield random.uniform(-max_number, max_number)

    control_sum = 0.0
    with open(filename, 'wt') as file_obj:
        for number in real_numbers(quantity, limit):
            file_obj.write(str(number) + "\n")
            control_sum += number

    return control_sum


def read_numbers_from_file(filename: str) -> float:
    """Read numbers from a file. Return their sum."""

    sum_of_numbers = 0.0
    with open(filename, 'rt') as file_obj:
        sum_of_numbers = sum(map(float, file_obj))

    return sum_of_numbers


if __name__ == '__main__':

    file_name = './10000-real-numbers.txt'

    print('\n----Saving numbers')
    ctl_sum = save_numbers_to_file(file_name)
    print(ctl_sum)

    print('\n----Reading numbers')
    num_sum = read_numbers_from_file(file_name)
    print(num_sum)
