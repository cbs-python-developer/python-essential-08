"""Homework 8 Task 3 implementation."""

import json
import pickle
from pprint import pprint

photo = {
    'cameras': [
        {
            'id': 1101,
            'brand': 'Canon',
            'name': 'EOS R5',
            'type': 'mirrorless',
            'sensor': '36 x 24 mm CMOS',
            'pixels': 'Approx. 45 megapixels',
            'price': 156000
        },
        {
            'id': 1102,
            'brand': 'Sony',
            'name': 'A7R V',
            'type': 'mirrorless',
            'sensor': '35mm full frame (35.7 x 23.8 mm), Exmor R CMOS sensor',
            'pixels': 'Approx. 62.5 megapixels',
            'price': 172000
        },
    ],
    'lenses': [
        {
            'id': 3101,
            'brand': 'Leica',
            'name': 'APO-Summicron-SL 75mm f/2 ASPH',
            'aperture setting range': '2-22',
            'bayonet': 'Leica L-bayonet',
            'sensor format': 'full-frame 35 mm',
            'price': 48000
        },
        {
            'id': 3102,
            'brand': 'Canon',
            'name': 'RF 85mm f/1.2L USM',
            'aperture setting range': '1.2-16',
            'bayonet': 'RF',
            'sensor format': 'full-frame 35 mm',
            'price': 74000
        },

    ],
    'printers': [
        {
            'id': 7101,
            'brand': 'Canon',
            'name': 'imagePROGRAF PRO-1000',
            'type': 'inks',
            'colors': 12,
            'format': 'A2',
            'price': 92000
        },
    ]
}

# --PICKLE
pickle_file = './goods.pickle'

with open(pickle_file, 'wb') as file_obj:
    pickle.dump(photo, file_obj)

with open(pickle_file, 'rb') as file_obj:
    pickle_data = pickle.load(file_obj)

print('\n----From PICKLE file:')
pprint(pickle_data)
print(type(pickle_data['cameras'][0]['id']))

# --JSON
json_file = './goods.json'

with open(json_file, 'wt') as file_obj:
    json.dump(photo, file_obj)

with open(json_file, 'rt') as file_obj:
    json_data = json.load(file_obj)

print('\n----From JSON file:')
pprint(json_data)
print(type(json_data['cameras'][0]['id']))
